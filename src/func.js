const getSum = (str1, str2) => {

    if (typeof str1 !== 'string' || typeof str2 !== 'string') return false;
    if (isNaN(+str1) === true || isNaN(+str2)) return false;
    return (+str1 + +str2).toString()

};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
    // add your implementation below
    let postCounter = 0;
    let commentsCounter = 0;

    for (let i = 0; i < listOfPosts.length; i++) {
        if (listOfPosts[i].author === authorName) {
            postCounter++;
        }

        if (listOfPosts[i].comments) {
            (listOfPosts[i].comments).forEach(comment => {
                if (comment.author === authorName) {
                    commentsCounter++
                }
            })
        }
    }
    return `Post:${postCounter},comments:${commentsCounter}`
};

const tickets = (people) => {
    // add your implementation below
    let answer = 0;
    let finalOutcome = 0;
    const getMaxOfArr = people => Math.max.apply(null, people)

    for (let i = 0; i < people.length; i++) {
        if (people[i] % 25 !== 0) {
            finalOutcome = "NO"
            break;
        }

        answer = people.reduce((sum, el) => {
            return sum += +el
        }, 0)

        finalOutcome = (answer - getMaxOfArr(people)) >= getMaxOfArr(people) ? 'YES' : "NO"

    }
    return finalOutcome;

};







module.exports = {getSum, getQuantityPostsByAuthor, tickets};
